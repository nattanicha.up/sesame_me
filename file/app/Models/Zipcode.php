<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Zipcode extends Model
{
    use HasFactory;

    protected $table = '005_ZIPCODE';
    protected $fillable = [
        'district_code',
        'zipcode',
    ];
}
