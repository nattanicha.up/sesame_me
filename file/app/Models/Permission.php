<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasFactory;

    protected $table = '101_USER_PERMISSION';
    protected $fillable = [
        'title',
        'description',
        'status',
    ];
}
