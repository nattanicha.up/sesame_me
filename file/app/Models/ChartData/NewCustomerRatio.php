<?php

namespace App\Models\ChartData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewCustomerRatio extends Model
{
    use HasFactory;

    protected $table = '807_NewCustomerRatio';
    protected $fillable = [
        'DateTime',
        'Ration_net',
        'Ration_buy',
        'Ration_sell',
        'Ration_pawn',
        'Ration_collect',
    ];
}
