<?php

namespace App\Models\ChartData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MontlyRevenue extends Model
{
    use HasFactory;

    protected $table = '801_MontlyRevenue';
    protected $fillable = [
        'InvoiceDate',
        'net',
        'pawn',
        'collect',
        'sell',
        'buy',
    ];
}
