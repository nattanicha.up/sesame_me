<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    use HasFactory;

    protected $table = '003_DISTRICT';
    protected $fillable = [
        'code',
        'title_th',
        'title_en',
        'geo_id',
        'province_id',
    ];
}
