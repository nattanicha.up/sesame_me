<?php

namespace App\Models\Gold;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gold extends Model
{
    use HasFactory;

    protected $table = '200_GOLDS';

    protected $fillable = [
        'category_id',
        'gold_type',
        'subcategory_id',
        'title',
        'weight',
        'weight_real',
        'unit_id',
        'size',
        'pic',
        'user_id',
        'update_by',
        'company_id',
    ];

    public function stock(){
        return $this->hasOne(TransectionStock::class,'gold_id','id');
    }

    public function type(){
        return $this->hasOne(GoldType::class,'id','gold_type');
    }

    public function category(){
        return $this->hasOne(GoldCategory::class,'id','category_id');
    }

    public function subcategory(){
        return $this->hasOne(GoldSubCategory::class,'id','subcategory_id');
    }

    public function units(){
        return $this->hasOne(GoldUnits::class,'id','unit_id');
    }

    public function price(){
        return $this->hasOne(TransectionStock::class,'gold_id','id');
    }
}
