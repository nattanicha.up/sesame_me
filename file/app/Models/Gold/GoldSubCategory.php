<?php

namespace App\Models\Gold;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GoldSubCategory extends Model
{
    use HasFactory;

    protected $table = '202_GOLDS_SUB_CATEGORY';
    protected $fillable = [
        'title',
        'status',
        'category_id',
    ];
}