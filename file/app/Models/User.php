<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    protected $table = '100_USERS';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'password',
        'name',
        'surname',
        'gender',
        'id_card',
        'permission_id',
        'facebook',
        'line',
        'email',
        'status',
        'pic',
        'user_id',
        'update_by',
        'company_id',
        'notifi_price',
        'notifi_sms',
        'acc_expire',
        'update_by',
        'company_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function permission(){
        return $this->hasOne(Permission::class,'id','permission_id');
    }

    public function gender(){
        return $this->hasOne(Gender::class,'id','gender');
    }

    public function company(){
        return $this->hasOne(Company::class,'id','company_id');
    }
}
