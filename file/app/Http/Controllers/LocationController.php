<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\Province;
use App\Models\Subdistrict;
use App\Models\Zipcode;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    public function province(){
        try{
            $data = Province::query()->get();
            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function district(Request $request){
        try{
            $data = District::query()
                ->where('province_id',$request->province_id)
                ->get();
            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function subdistrict(Request $request){
        try{
            $data = Subdistrict::query()
                ->where('district_id',$request->district_id)
                ->get();
            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function zipcode(Request $request){
        try{
            $subdistrict = Subdistrict::query()
                ->where('id',$request->subdistrict_id)
                ->first();

            $data = Zipcode::query()
                ->where('district_code',$subdistrict->code)
                ->first();
            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }
}
