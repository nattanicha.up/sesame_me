<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use Illuminate\Http\Request;

class GeneralController extends Controller
{
    public function bankList(){
        try{
            $data = Bank::query()->get();
            return response()->json([
                'error' => 0,
                'data' => $data,
            ],200);

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'messages' => $e,
            ], 200);
        }
    }
}
