<?php

namespace App\Http\Controllers\Gold;

use App\GoldBuy;
use App\Http\Controllers\Controller;
use App\Models\Gold\Gold;
use App\Models\Gold\GoldSubCategory;
use App\Models\Gold\TransectionStock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;

class GoldController extends Controller
{
    public function goldSubCategory(Request $request)
    {
        try {
            if($request->type == 1) {
            $data = GoldSubCategory::query()
                ->select('id', 'title')
                ->where('id', '<>', '0')
                ->where('id', '<>', '1')
                ->where('category_id','=',$request->type)
                ->get();
            }
            else if($request->type == 4) {
                $data = GoldSubCategory::query()
                    ->select('id', 'title')
                    ->where('id', '<>', '0')
                    ->where('id', '<>', '1')
                    ->where('category_id','=',$request->type)
                    ->get();
            }

            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'messages' => $e,
            ], '200');
        }
    }

    public function goldStore(Request $request)
    {
        try{
            if($request->gold_category == 1) {
                if($request->gold_type == 1) {
                    $rules = [
                        'gold_pattern' => 'required',
                        'gold_weight' => 'required',
                        'gold_weight_actual' => 'required',
                        'gold_cost' => 'required',
                        'gold_smith_charge' => 'required',
                    ];

                    $message = [
                        '*.required' => 'โปรดระบุ',
                    ];

                    if($request->file('gold_image') != null) {
                        $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                        $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                    }

                    $validator = Validator::make($request->all(), $rules, $message);

                    if ($validator->fails()) {
                        return response()->json([
                            'error' => 1,
                            'messages' => $validator->messages(),
                        ], 200);
                    }

                    if($request->gold_list == null) {
                        return response()->json([
                            'error' => 1,
                            'messages' => ['gold_list' => ['กดปุ่ม เพิ่ม เพื่อระบุ ขนาด/ความยาว และ จำนวน/ชิ้น']],
                        ], 200);
                    }
                    else {
                        foreach($request->gold_list as $row) {
                            if($row['gold_size'] == null || $row['gold_amount'] == null) {
                                return response()->json([
                                    'error' => 1,
                                    'messages' => ['gold_list' => ['โปรดระบุ ขนาด/ความยาว และ จำนวน/ชิ้น ให้ครบทุกช่อง หรือ ลบช่องที่ไม่ได้ใช้งานออก']],
                                ], 200);
                            }
                        }
                    }

                    foreach($request->gold_list as $row) {
                        $gold = Gold::create();
                        $gold->category_id = $request->gold_category;
                        $gold->gold_type = $request->gold_type;
                        $gold->subcategory_id = $request->gold_sub_category;
                        $gold->title = $request->gold_pattern;
                        $gold->weight = $request->gold_weight;
                        $gold->weight_real = $request->gold_weight_actual;
                        $gold->unit_id = $request->gold_unit;
                        $gold->size = $row['gold_size'];

                        if ($request->file('gold_image') != null) {
                            $extension = $request->file('gold_image')->guessExtension();
                            $name = 'G' . Auth::user()->company_id . date('YmdHis'). $this->random_numb(5);
                            $img = Image::make($_FILES['gold_image']['tmp_name']);
                            $img->fit(800, 640, function ($constraint) {
                                $constraint->upsize();
                            });
                            $img->save(public_path('_uploads/_products/') . $name . '.' . $extension);
                            $gold->pic = $name . '.' . $extension;
                        }

                        $gold->user_id = Auth::user()->id;
                        $gold->update_by = Auth::user()->id;
                        $gold->company_id = Auth::user()->company_id;
                        $gold->save();

                        for ($i = 0; $i < $row['gold_amount']; $i++) {
                            $stock = TransectionStock::create();
                            $stock->gold_id = $gold->id;

                            $generate = true;
                            while($generate) {
                                $sku = 'G'. date('YmdHis'). $this->random_numb(5);
                                $barcode = TransectionStock::query()->where('sku', '=', $sku)->count();
                                if($barcode == 0){
                                    $generate = false;
                                }
                            }

                            $stock->sku = $sku;
                            $stock->price_income = $request->gold_cost;
                            $stock->price_goldsmith = $request->gold_smith_charge;
                            $stock->status = 1;
                            $stock->user_id = Auth::user()->id;
                            $stock->update_by = Auth::user()->id;
                            $stock->company_id = Auth::user()->company_id;
                            $stock->save();
                        }
                    }
                }
                else if($request->gold_type == 2) {
                    $rules = [
                        'gold_pattern' => 'required',
                        'gold_weight' => 'required',
                        'gold_weight_actual' => 'required',
                        'gold_cost' => 'required',
                        'gold_smith_charge' => 'required',
                        'gold_amount' => 'required',
                    ];

                    $message = [
                        '*.required' => 'โปรดระบุ',
                    ];

                    if($request->file('gold_image') != null) {
                        $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                        $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                    }

                    $validator = Validator::make($request->all(), $rules, $message);

                    if ($validator->fails()) {
                        return response()->json([
                            'error' => 1,
                            'messages' => $validator->messages(),
                        ], 200);
                    }

                    $gold = Gold::create();
                    $gold->category_id = $request->gold_category;
                    $gold->gold_type = $request->gold_type;
                    $gold->title = $request->gold_pattern;
                    $gold->weight = $request->gold_weight;
                    $gold->weight_real = $request->gold_weight_actual;
                    $gold->unit_id = $request->gold_unit;

                    if ($request->file('gold_image') != null) {
                        $extension = $request->file('gold_image')->guessExtension();
                        $name = 'G' . Auth::user()->company_id . date('YmdHis'). $this->random_numb(5);
                        $img = Image::make($_FILES['gold_image']['tmp_name']);
                        $img->fit(800, 640, function ($constraint) {
                            $constraint->upsize();
                        });
                        $img->save(public_path('_uploads/_products/') . $name . '.' . $extension);
                        $gold->pic = $name . '.' . $extension;
                    }

                    $gold->user_id = Auth::user()->id;
                    $gold->update_by = Auth::user()->id;
                    $gold->company_id = Auth::user()->company_id;
                    $gold->save();

                    for ($i = 0; $i < $request->gold_amount; $i++) {
                        $stock = TransectionStock::create();
                        $stock->gold_id = $gold->id;

                        $generate = true;
                        while($generate) {
                            $sku = 'G'. date('YmdHis'). $this->random_numb(5);
                            $barcode = TransectionStock::query()->where('sku', '=', $sku)->count();
                            if($barcode == 0){
                                $generate = false;
                            }
                        }

                        $stock->sku = $sku;
                        $stock->price_income = $request->gold_cost;
                        $stock->price_goldsmith = $request->gold_smith_charge;
                        $stock->status = 1;
                        $stock->user_id = Auth::user()->id;
                        $stock->update_by = Auth::user()->id;
                        $stock->company_id = Auth::user()->company_id;
                        $stock->save();
                    }
                }
            }
            else if($request->gold_category == 2) {
                if($request->gold_type == 1) {
                    $rules = [
                        'gold_pattern' => 'required',
                        'gold_weight_actual' => 'required',
                        'gold_cost' => 'required',
                        'gold_smith_charge' => 'required',
                    ];

                    $message = [
                        '*.required' => 'โปรดระบุ',
                    ];

                    if($request->file('gold_image') != null) {
                        $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                        $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                    }

                    $validator = Validator::make($request->all(), $rules, $message);

                    if ($validator->fails()) {
                        return response()->json([
                            'error' => 1,
                            'messages' => $validator->messages(),
                        ], 200);
                    }

                    if($request->gold_list == null) {
                        return response()->json([
                            'error' => 1,
                            'messages' => ['gold_list' => ['กดปุ่ม เพิ่ม เพื่อระบุ ขนาด/ความยาว และ จำนวน/ชิ้น']],
                        ], 200);
                    }
                    else {
                        foreach($request->gold_list as $row) {
                            if($row['gold_size'] == null || $row['gold_amount'] == null) {
                                return response()->json([
                                    'error' => 1,
                                    'messages' => ['gold_list' => ['โปรดระบุ ขนาด/ความยาว และ จำนวน/ชิ้น ให้ครบทุกช่อง หรือ ลบช่องที่ไม่ได้ใช้งานออก']],
                                ], 200);
                            }
                        }
                    }

                    foreach($request->gold_list as $row) {
                        $gold = Gold::create();
                        $gold->category_id = $request->gold_category;
                        $gold->gold_type = $request->gold_type;
                        $gold->subcategory_id = $request->gold_sub_category;
                        $gold->title = $request->gold_pattern;
                        $gold->weight_real = $request->gold_weight_actual;
                        $gold->size = $row['gold_size'];

                        if ($request->file('gold_image') != null) {
                            $extension = $request->file('gold_image')->guessExtension();
                            $name = 'G' . Auth::user()->company_id . date('YmdHis'). $this->random_numb(5);
                            $img = Image::make($_FILES['gold_image']['tmp_name']);
                            $img->fit(800, 640, function ($constraint) {
                                $constraint->upsize();
                            });
                            $img->save(public_path('_uploads/_products/') . $name . '.' . $extension);
                            $gold->pic = $name . '.' . $extension;
                        }

                        $gold->user_id = Auth::user()->id;
                        $gold->update_by = Auth::user()->id;
                        $gold->company_id = Auth::user()->company_id;
                        $gold->save();

                        for ($i = 0; $i < $row['gold_amount']; $i++) {
                            $stock = TransectionStock::create();
                            $stock->gold_id = $gold->id;

                            $generate = true;
                            while($generate) {
                                $sku = 'G'. date('YmdHis'). $this->random_numb(5);
                                $barcode = TransectionStock::query()->where('sku', '=', $sku)->count();
                                if($barcode == 0){
                                    $generate = false;
                                }
                            }

                            $stock->sku = $sku;
                            $stock->price_income = $request->gold_cost;
                            $stock->price_goldsmith = $request->gold_smith_charge;
                            $stock->status = 1;
                            $stock->user_id = Auth::user()->id;
                            $stock->update_by = Auth::user()->id;
                            $stock->company_id = Auth::user()->company_id;
                            $stock->save();
                        }
                    }
                }
                else if($request->gold_type == 2) {
                    $rules = [
                        'gold_pattern' => 'required',
                        'gold_weight_actual' => 'required',
                        'gold_cost' => 'required',
                        'gold_smith_charge' => 'required',
                        'gold_amount' => 'required',
                    ];

                    $message = [
                        '*.required' => 'โปรดระบุ',
                    ];

                    if($request->file('gold_image') != null) {
                        $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                        $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                    }

                    $validator = Validator::make($request->all(), $rules, $message);

                    if ($validator->fails()) {
                        return response()->json([
                            'error' => 1,
                            'messages' => $validator->messages(),
                        ], 200);
                    }

                    $gold = Gold::create();
                    $gold->category_id = $request->gold_category;
                    $gold->gold_type = $request->gold_type;
                    $gold->title = $request->gold_pattern;
                    $gold->weight_real = $request->gold_weight_actual;

                    if ($request->file('gold_image') != null) {
                        $extension = $request->file('gold_image')->guessExtension();
                        $name = 'G' . Auth::user()->company_id . date('YmdHis'). $this->random_numb(5);
                        $img = Image::make($_FILES['gold_image']['tmp_name']);
                        $img->fit(800, 640, function ($constraint) {
                            $constraint->upsize();
                        });
                        $img->save(public_path('_uploads/_products/') . $name . '.' . $extension);
                        $gold->pic = $name . '.' . $extension;
                    }

                    $gold->user_id = Auth::user()->id;
                    $gold->update_by = Auth::user()->id;
                    $gold->company_id = Auth::user()->company_id;
                    $gold->save();

                    for ($i = 0; $i < $request->gold_amount; $i++) {
                        $stock = TransectionStock::create();
                        $stock->gold_id = $gold->id;

                        $generate = true;
                        while($generate) {
                            $sku = 'G'. date('YmdHis'). $this->random_numb(5);
                            $barcode = TransectionStock::query()->where('sku', '=', $sku)->count();
                            if($barcode == 0){
                                $generate = false;
                            }
                        }

                        $stock->sku = $sku;
                        $stock->price_income = $request->gold_cost;
                        $stock->price_goldsmith = $request->gold_smith_charge;
                        $stock->status = 1;
                        $stock->user_id = Auth::user()->id;
                        $stock->update_by = Auth::user()->id;
                        $stock->company_id = Auth::user()->company_id;
                        $stock->save();
                    }
                }
            }
            else if($request->gold_category == 3) {
                if($request->gold_type == 1) {
                    $rules = [
                        'gold_pattern' => 'required',
                        'gold_weight_actual' => 'required',
                        'gold_cost' => 'required',
                        'gold_smith_charge' => 'required',
                    ];

                    $message = [
                        '*.required' => 'โปรดระบุ',
                    ];

                    if($request->file('gold_image') != null) {
                        $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                        $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                    }

                    $validator = Validator::make($request->all(), $rules, $message);

                    if ($validator->fails()) {
                        return response()->json([
                            'error' => 1,
                            'messages' => $validator->messages(),
                        ], 200);
                    }

                    if($request->gold_list == null) {
                        return response()->json([
                            'error' => 1,
                            'messages' => ['gold_list' => ['กดปุ่ม เพิ่ม เพื่อระบุ ขนาด/ความยาว และ จำนวน/ชิ้น']],
                        ], 200);
                    }
                    else {
                        foreach($request->gold_list as $row) {
                            if($row['gold_size'] == null || $row['gold_amount'] == null) {
                                return response()->json([
                                    'error' => 1,
                                    'messages' => ['gold_list' => ['โปรดระบุ ขนาด/ความยาว และ จำนวน/ชิ้น ให้ครบทุกช่อง หรือ ลบช่องที่ไม่ได้ใช้งานออก']],
                                ], 200);
                            }
                        }
                    }

                    foreach($request->gold_list as $row) {
                        $gold = Gold::create();
                        $gold->category_id = $request->gold_category;
                        $gold->gold_type = $request->gold_type;
                        $gold->subcategory_id = $request->gold_sub_category;
                        $gold->title = $request->gold_pattern;
                        $gold->weight_real = $request->gold_weight_actual;
                        $gold->size = $row['gold_size'];

                        if ($request->file('gold_image') != null) {
                            $extension = $request->file('gold_image')->guessExtension();
                            $name = 'G' . Auth::user()->company_id . date('YmdHis'). $this->random_numb(5);
                            $img = Image::make($_FILES['gold_image']['tmp_name']);
                            $img->fit(800, 640, function ($constraint) {
                                $constraint->upsize();
                            });
                            $img->save(public_path('_uploads/_products/') . $name . '.' . $extension);
                            $gold->pic = $name . '.' . $extension;
                        }

                        $gold->user_id = Auth::user()->id;
                        $gold->update_by = Auth::user()->id;
                        $gold->company_id = Auth::user()->company_id;
                        $gold->save();

                        for ($i = 0; $i < $row['gold_amount']; $i++) {
                            $stock = TransectionStock::create();
                            $stock->gold_id = $gold->id;

                            $generate = true;
                            while($generate) {
                                $sku = 'G'. date('YmdHis'). $this->random_numb(5);
                                $barcode = TransectionStock::query()->where('sku', '=', $sku)->count();
                                if($barcode == 0){
                                    $generate = false;
                                }
                            }

                            $stock->sku = $sku;
                            $stock->price_income = $request->gold_cost;
                            $stock->price_goldsmith = $request->gold_smith_charge;
                            $stock->status = 1;
                            $stock->user_id = Auth::user()->id;
                            $stock->update_by = Auth::user()->id;
                            $stock->company_id = Auth::user()->company_id;
                            $stock->save();
                        }
                    }
                }
                else if($request->gold_type == 2) {
                    $rules = [
                        'gold_pattern' => 'required',
                        'gold_weight_actual' => 'required',
                        'gold_cost' => 'required',
                        'gold_smith_charge' => 'required',
                        'gold_amount' => 'required',
                    ];

                    $message = [
                        '*.required' => 'โปรดระบุ',
                    ];

                    if($request->file('gold_image') != null) {
                        $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                        $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                    }

                    $validator = Validator::make($request->all(), $rules, $message);

                    if ($validator->fails()) {
                        return response()->json([
                            'error' => 1,
                            'messages' => $validator->messages(),
                        ], 200);
                    }

                    $gold = Gold::create();
                    $gold->category_id = $request->gold_category;
                    $gold->gold_type = $request->gold_type;
                    $gold->title = $request->gold_pattern;
                    $gold->weight_real = $request->gold_weight_actual;

                    if ($request->file('gold_image') != null) {
                        $extension = $request->file('gold_image')->guessExtension();
                        $name = 'G' . Auth::user()->company_id . date('YmdHis'). $this->random_numb(5);
                        $img = Image::make($_FILES['gold_image']['tmp_name']);
                        $img->fit(800, 640, function ($constraint) {
                            $constraint->upsize();
                        });
                        $img->save(public_path('_uploads/_products/') . $name . '.' . $extension);
                        $gold->pic = $name . '.' . $extension;
                    }

                    $gold->user_id = Auth::user()->id;
                    $gold->update_by = Auth::user()->id;
                    $gold->company_id = Auth::user()->company_id;
                    $gold->save();

                    for ($i = 0; $i < $request->gold_amount; $i++) {
                        $stock = TransectionStock::create();
                        $stock->gold_id = $gold->id;

                        $generate = true;
                        while($generate) {
                            $sku = 'G'. date('YmdHis'). $this->random_numb(5);
                            $barcode = TransectionStock::query()->where('sku', '=', $sku)->count();
                            if($barcode == 0){
                                $generate = false;
                            }
                        }

                        $stock->sku = $sku;
                        $stock->price_income = $request->gold_cost;
                        $stock->price_goldsmith = $request->gold_smith_charge;
                        $stock->status = 1;
                        $stock->user_id = Auth::user()->id;
                        $stock->update_by = Auth::user()->id;
                        $stock->company_id = Auth::user()->company_id;
                        $stock->save();
                    }
                }
            }
            else if($request->gold_category == 4) {
                $rules = [
                    'gold_pattern' => 'required',
                    'gold_weight_actual' => 'required',
                    'gold_cost' => 'required',
                    'gold_smith_charge' => 'required',
                ];

                $message = [
                    '*.required' => 'โปรดระบุ',
                ];

                if($request->file('gold_image') != null) {
                    $rules['gold_image'] = 'image|mimes:jpg,png,jpeg,gif,svg';
                    $message['gold_image.*'] = 'การอัพโหลดไฟล์รองรับนามสกุล jpg,png,jpeg,gif,svg เท่านั้น';
                }

                $validator = Validator::make($request->all(), $rules, $message);

                if ($validator->fails()) {
                    return response()->json([
                        'error' => 1,
                        'messages' => $validator->messages(),
                    ], 200);
                }

                if($request->gold_list == null) {
                    return response()->json([
                        'error' => 1,
                        'messages' => ['gold_list' => ['กดปุ่ม เพิ่ม เพื่อระบุ ขนาด/ความยาว และ จำนวน/ชิ้น']],
                    ], 200);
                }
                else {
                    foreach($request->gold_list as $row) {
                        if($row['gold_size'] == null || $row['gold_amount'] == null) {
                            return response()->json([
                                'error' => 1,
                                'messages' => ['gold_list' => ['โปรดระบุ ขนาด/ความยาว และ จำนวน/ชิ้น ให้ครบทุกช่อง หรือ ลบช่องที่ไม่ได้ใช้งานออก']],
                            ], 200);
                        }
                    }
                }

                foreach($request->gold_list as $row) {
                    $gold = Gold::create();
                    $gold->category_id = $request->gold_category;
                    $gold->subcategory_id = $request->gold_sub_category;
                    $gold->title = $request->gold_pattern;
                    $gold->weight_real = $request->gold_weight_actual;
                    $gold->size = $row['gold_size'];

                    if ($request->file('gold_image') != null) {
                        $extension = $request->file('gold_image')->guessExtension();
                        $name = 'G' . Auth::user()->company_id . date('YmdHis'). $this->random_numb(5);
                        $img = Image::make($_FILES['gold_image']['tmp_name']);
                        $img->fit(800, 640, function ($constraint) {
                            $constraint->upsize();
                        });
                        $img->save(public_path('_uploads/_products/') . $name . '.' . $extension);
                        $gold->pic = $name . '.' . $extension;
                    }

                    $gold->user_id = Auth::user()->id;
                    $gold->update_by = Auth::user()->id;
                    $gold->company_id = Auth::user()->company_id;
                    $gold->save();

                    for ($i = 0; $i < $row['gold_amount']; $i++) {
                        $stock = TransectionStock::create();
                        $stock->gold_id = $gold->id;

                        $generate = true;
                        while($generate) {
                            $sku = 'G'. date('YmdHis'). $this->random_numb(5);
                            $barcode = TransectionStock::query()->where('sku', '=', $sku)->count();
                            if($barcode == 0){
                                $generate = false;
                            }
                        }

                        $stock->sku = $sku;
                        $stock->price_income = $request->gold_cost;
                        $stock->price_goldsmith = $request->gold_smith_charge;
                        $stock->status = 1;
                        $stock->user_id = Auth::user()->id;
                        $stock->update_by = Auth::user()->id;
                        $stock->company_id = Auth::user()->company_id;
                        $stock->save();
                    }
                }
            }

            return response()->json([
                'error' => 0,
                'messages' => "เพิ่มข้อมูลเรียบร้อย",
            ],200);

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'messages' => $e,
            ], 200);
        }
    }


    public function random_numb($len)
    {
        srand((double)microtime()*10000000);
        $chars = "0123456789";
        $ret_str = "";
        $num = strlen($chars);
        for($i = 0; $i < $len; $i++)
        {
            $ret_str.= $chars[rand()%$num];
            $ret_str.="";
        }
        return $ret_str;
    }

    public function goldStock(){
        
        try{
            $data = Gold::query()
                ->when(request()->filled('category'), function($q){
                    $q->where('category_id', request('category'));
                })
                ->when(request()->filled('subcategory'), function($q){
                    $q->where('subcategory_id', request('subcategory'));
                })
                ->when(request()->filled('txt'), function($q){
                    $q->where('title', 'like', '%'. request('txt'). '%');
                })
                ->when(request()->filled('weight'), function($q){
                    $q->where('weight', 'like', '%'. request('weight'). '%');
                })
                ->when(request()->filled('units'), function($q){
                    $q->where('unit_id', request('units'));
                })
                ->when(request()->filled('size'), function($q){
                    $q->where('size', 'like', '%'. request('size'). '%');
                })
                ->with('type')
                ->with('category')
                ->with('subcategory')                
                ->with('units')
                ->with(['stock' => function ($query) {
                    $query->select('gold_id')
                        ->where('status','=','1')
                        ->groupBy('gold_id')
                        ->selectRaw('FORMAT(count(*), 0) as count');
                }])
                ->with(['price' => function ($query) {
                    $query->select('gold_id')
                        ->groupBy('gold_id')
                        ->selectRaw('FORMAT(avg(price_income), 0) as income')
                        ->selectRaw('FORMAT(avg(price_goldsmith), 0) as smith');
                }])
                ->paginate(4);
            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    //new
    public function getPriceToday(){
            $price = DB::connection('mysql2')->table("899_GOLD_PRICE")->orderByDesc('id')->get();
            return $price;
    }    

    public function getSubCategory($subcategory_id){
        return DB::table('202_GOLDS_SUB_CATEGORY')->where('id',$subcategory_id)->first();
    }

    public function getGoldUnit($unit_id){
        return DB::table('204_GOLDS_UNIT')->where('id',$unit_id)->first();
    }

    public function checkStock($id){
        $stock = DB::table('430_TRANSECTION_STOCK')->where('gold_id', '=', $id)->where('status', '=', 1)->get();
        return count($stock);
    }

    public function getPriceAverage($gold_id){
        $stock = DB::table('430_TRANSECTION_STOCK')->where('gold_id', '=', $gold_id);
        $stock_sum = $stock->sum('price_income');
        $stock_count = count($stock->get());
        return number_format($stock_sum/$stock_count, 2);
    }

    public function getSmithAverage($gold_id){
        $stock = DB::table('430_TRANSECTION_STOCK')->where('gold_id', '=', $gold_id);
        $stock_sum = $stock->sum('price_goldsmith');
        $stock_count = count($stock->get());
        return number_format($stock_sum/$stock_count, 2);
    }

    public function getGold(Request $request) {
        try {
            $data = Gold::where('id', $request->id)->first();
            $error = false;
            // if ($data->subcategory_id != null){
            //     $subcategory = $this->getSubCategory($data->subcategory_id);
            // }else{
            //     $subcategory = "";
            // }

            $subcategory = $data->subcategory;
            // dd($subcategory);

            // if ($data->unit_id != null){
            //     $unit = $this->getGoldUnit($data->unit_id);
            // }else{
            //     $unit = "";
            // }

            $unit = $data->units;

            $amount = $this->checkStock($data->id);


            $weight_unit = 1;
            if($data->unit_id == 1) { $weight_unit *= 0.25; }
            $price = $this->getPriceToday()[0]->goldbar_sell * ($weight_unit * $data->weight);
            // dd($price);


            $smith = $this->getSmithAverage($data->id);
            $stock = 0;

                $amount = $this->checkStock($data->id);
                $price = 0;
                $smith = 0;
                if($amount > 0) {
                    $price = $this->getPriceAverage($data->id);
                    $smith = $this->getSmithAverage($data->id);
                    if ($request->stock != null){
                        $stock = TransectionStock::query()->where('id',$request->stock)->first();
                    }
                }

            return response()->json([
                'error' => 0,
                'data' => $data,
                'subcategory' => $subcategory,
                'price' => $price,
                'amount' => $amount,
                'smith' => $smith,
                'message' => 'สำเร็จ'
            ],'200');
        }
        catch(\Throwable $e) {
            $error = "catch";
            $message = $e;
            return response()->json([
                'error' => $error,
                'message' => $message
            ],'200');
        }
    }

    function randomBarcode(){

        srand((double)microtime()*10000000);
        $chars = "0123456789";
        $ret_str = "";
        $num = strlen($chars);
        for($i = 0; $i < 2; $i++)
        {
            $ret_str.= $chars[rand()%$num];
            $ret_str.="";
        }
        return $ret_str;
    
    }

    // public function add(Request $request, $id)
    // {
    //     // dd($id);
    //     // dd($request->all());
    //     try {
    //         $validator = Validator::make($request->all(), [
    //             'price_income' => ['required'],
    //             'price_goldsmith' => ['required'],
    //             'amount' => ['required']
    //         ]);

    //         if ($validator->fails()){
    //             return response()->json([
    //                 'error' => true,
    //                 'messages' => 'โปรดระบุข้อมูลให้ครบถ้วน'
    //             ], 200);
    //         }

    //         for ($i = 0; $i < $request->amount; $i++) {
    //             $stock = TransectionStock::create();
    //             $stock->gold_id = $id;
    //             $stock->sku = 'G' . Auth::user()->company_id . date('ymdHis'). $i = $this->randomBarcode();
    //             $stock->price_income = $request->price_income;
    //             $stock->price_goldsmith = $request->price_goldsmith;
    //             $stock->status = 1;
    //             $stock->user_id = Auth::user()->id;
    //             $stock->update_by = Auth::user()->id;
    //             $stock->company_id = Auth::user()->company_id;
    //             $stock->save();
    //         }

    //         return response()->json([
    //             'error' => false,
    //             'messages' => 'บันทึกข้อมูลเรียบร้อย'
    //         ], 200);
    //     }
    //     catch (\Throwable $e) {
    //         return response()->json([
    //             'error' => true,
    //             'messages' => $e
    //         ],200);
    //     }
    // }

    public function add(Request $request, $id)
    {

        try {
            $validator = Validator::make($request->all(), [
                'price_income' => ['required'],
                'price_goldsmith' => ['required'],
                'amount' => ['required']
            ]);

            if ($validator->fails()){
                return response()->json([
                    'error' => true,
                    'messages' => 'โปรดระบุข้อมูลให้ครบถ้วน'
                ], 200);
            }

            for ($i = 0; $i < $request->amount; $i++) {
                $stock = TransectionStock::create();
                $stock->gold_id = $id;
                $stock->sku = $this->generateSKU();
                // $stock->sku = 'G' . Auth::user()->company_id . date('ymdHis').$this->randomBarcode();
                // $i=$this->randomBarcode();
                $stock->price_income = $request->price_income;
                $stock->price_goldsmith = $request->price_goldsmith;
                $stock->status = 1;
                $stock->user_id = Auth::user()->id;
                $stock->update_by = Auth::user()->id;
                $stock->company_id = Auth::user()->company_id;
                $stock->save();
            }

            return response()->json([
                'error' => false,
                'messages' => 'บันทึกข้อมูลเรียบร้อย'
            ], 200);
        }
        catch (\Throwable $e) {
            return response()->json([
                'error' => true,
                'messages' => $e
            ],200);
        }
    }

    public function generateSKU(){
        $generate = true;
        while($generate) {
            $barcode_number = 'G' . Auth::user()->company_id . date('ymdHis').$this->randomBarcode();
            $barcode = TransectionStock::query()->where('sku',$barcode_number)->count();
            if ($barcode == 0){
                $generate = false;
            }
        }
        return($barcode_number);
    }

    public function edit(Request $request, $id) { 
        
        // dd($request->all());

        try {
            $validator = Validator::make($request->all(), [
                'title_edit' => ['required'],
                'weight_edit' => ['required'],
                'weight_real_edit' => ['required'],
                'size_edit' => ['required']
            ],[
                '*.required' => 'โปรดระบุข้อมูล',
            ]);

            if ($validator->fails()){
                return response()->json([
                    'error' => true,
                    'messages' => 'โปรดระบุข้อมูลให้ครบถ้วน'
                ], 200);
            }

            // dd("บันทึกข้อมูลเรียบร้อย");

            $gold = Gold::where('id', $id)->first();
            $gold->title = $request->title_edit;
            $gold->subcategory_id = $request->subcategory_id_edit;
            $gold->weight = $request->weight_edit;
            $gold->weight_real = $request->weight_real_edit;
            $gold->unit_id = $request->unit_id_edit;
            $gold->size = $request->size_edit;
            if($request->file('pic') != '') {
                if($gold->pic != '') {
                    unlink(public_path('_uploads/_products/'. $gold->pic));
                }
                $extension = $request->file('pic')->guessExtension();
                $name = 'G' . Auth::user()->company_id . date('YmdHIs');
                $img = Image::make($_FILES['pic']['tmp_name']);
                $img->fit(800,800, function ($constraint) {
                    $constraint->upsize();
                });
                $img->save(public_path('_uploads/_products/') . $name . '.' . $extension);
                $gold->pic = $name . '.' . $extension;
            }
            $gold->update_by = Auth::user()->id;
            $gold->company_id = Auth::user()->company_id;
            $gold->save();

            return response()->json([
                'error' => false,
                'messages' => 'บันทึกข้อมูลเรียบร้อย'
            ], 200);
        }
        catch (\Throwable $e) {
            return response()->json([
                'error' => true,
                'messages' => $e
            ],200);
        }
    }

    public function getGoldDelete(Request $request) {
        $date_start = str_replace("T", " ", $request->date_start). " 00:00:00";
        $date_end = str_replace("T", " ", $request->date_end). " 23:59:59";
        // dd($request->all());
        try {
            $data = TransectionStock::where('gold_id', $request->gold_id)
                ->where('status', 1)
                ->whereBetween('created_at', [$date_start, $date_end])
                ->get();
            if ($data){
                $error = false;
                return response()->json([
                    'error' => $error,
                    'data' => $data
                ],'200');                
            }else{
                $error = true;
                $message = 'ไม่พบข้อมูล';
                return response()->json([
                    'error' => $error,
                    'message' => $message
                ],'200');
            }
        }
        catch(\Throwable $e) {
            $error = true;
            $message = $e;
            return response()->json([
                'error' => $error,
                'message' => $message
            ],'200');
        }
    }

    public function delete(Request $request) {
        try {
            if(isset($request->sku_delete)) {
                $count = count($request->sku_delete);
                if ($count > 0) {
                    for ($i = 0; $i < $count; $i++) {
                        TransectionStock::where('id', $request->sku_delete[$i])->delete();
                    }
                    $error = false;
                    $message = 'ลบข้อมูลเรียบร้อย';
                    return response()->json([
                        'error' => $error,
                        'message' => $message
                    ], '200');                    
                } else {
                    $error = true;
                    $message = 'โปรดเลือกสินค้าที่ต้องการลบอย่างน้อย 1 รายการ';
                    return response()->json([
                        'error' => $error,
                        'message' => $message
                    ], '200');                    
                }
            }
            else {
                $error = true;
                $message = 'โปรดเลือกสินค้าที่ต้องการลบอย่างน้อย 1 รายการ';
                return response()->json([
                    'error' => $error,
                    'message' => $message
                ], '200');                
            }
         }
         catch(\Throwable $e) {
             $error = true;
             $message = $e;
             return response()->json([
                 'error' => $error,
                 'message' => $message
             ],'200');
         }
     }
     
}
