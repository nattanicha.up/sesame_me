<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChangeGoldController extends Controller
{
    public function index()
    {
        $pageConfigs = [
            'pageClass' => 'ecommerce-application',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Checkout"]
        ];

        return view('/content/06_change_gold/changegold', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    public function changeGoldInvoice()
    {
        $pageConfigs = ['pageHeader' => false];

        return view('/content/06_change_gold/changegold-invoice', ['pageConfigs' => $pageConfigs]);
    }
}
