<?php

namespace App\Http\Controllers;

use App\Models\ChartData\ChurnRate;
use App\Models\ChartData\CustomerLifeTimeValue;
use App\Models\ChartData\CustomerSegmentation;
use App\Models\ChartData\LowHighSpender;
use App\Models\ChartData\MonthlyActiveCustomers;
use App\Models\ChartData\MonthlyOrderAverage;
use App\Models\ChartData\MonthlyTotalNumberOfOrder;
use App\Models\ChartData\MontlyRevenue;
use App\Models\ChartData\NewCustomerRatio;
use App\Models\ChartData\PredictingNextPurchaseDay;
use App\Models\ChartData\PredictingSales;
use Illuminate\Http\Request;

class ReportAdministatorController extends Controller
{
    public function income_report()
    {
        $maxSpender = LowHighSpender::max('Revenue');

        return view('/content/08_Report/incomereport', compact('maxSpender'));
    }

    public function predictingSales()
    {
        try{
            $data = PredictingSales::query()
                ->whereNotNull('Predicted')
//                ->where('Predicted', '=', 0)
                ->select(
                    PredictingSales::raw('DATE_FORMAT(InvoiceDate,\'%b %Y\') as date'),
                    PredictingSales::raw('FORMAT(Predicted, 0) as predicted')
                )->get();
            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function monthlyRevenue()
    {
        try{
            $data = MontlyRevenue::query()
//                ->where('revenue', '=', 0)
                ->select(
                    PredictingSales::raw('DATE_FORMAT(InvoiceDate,\'%b %Y\') as date'),
                    PredictingSales::raw('FORMAT(net, 0) as revenue')
                )->get();
            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function averageRevenue()
    {
        try{
            $data = MonthlyOrderAverage::query()
//                ->where('net', '=', 0)
                ->select(
                    MonthlyOrderAverage::raw('DATE_FORMAT(InvoiceDate,\'%b %Y\') as date'),
                    MonthlyOrderAverage::raw('FORMAT(net, 0) as net'),
                    'InvoiceDate'
                )
                ->with(['order' => function ($query) {
                    $query->select('InvoiceDate')->selectRaw('FORMAT(net, 0) as net');
                }])->get();
            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function customerLifeTimeValue()
    {
        try{
            $data = number_format(CustomerLifeTimeValue::avg('m6_Revenue'), 0);
            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function LowHighSpender()
    {
        try{
            $data = LowHighSpender::query()
                ->select(
                    'CustomerID','Revenue',
                    LowHighSpender::raw('FORMAT(Revenue, 0) as spend')
                )
                ->orderBy('Revenue', 'desc')
                ->get();
            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function monthlyActiveCustomer()
    {
        try{
            $data = MonthlyActiveCustomers::query()
//                ->where('net', '=', 0)
                ->select(
                    MonthlyActiveCustomers::raw('DATE_FORMAT(InvoiceDate,\'%b %Y\') as date'),
                    MonthlyActiveCustomers::raw('FORMAT(buy, 0) as buy'),
                    MonthlyActiveCustomers::raw('FORMAT(sell, 0) as sell'),
                    MonthlyActiveCustomers::raw('FORMAT(pawn, 0) as pawn'),
                    MonthlyActiveCustomers::raw('FORMAT(collect, 0) as collect')
                )
                ->get();
            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function churnRate()
    {
        try{
            $data = ChurnRate::query()
//                ->where('ChurnRate_net', '=', 0)
                ->select(
                    ChurnRate::raw('DATE_FORMAT(DateTime,\'%Y-%m-%d\') as date'),
                    'ChurnRate_net'
                )
                ->get();
            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function newCustomerRatio()
    {
        try{
            $data = NewCustomerRatio::query()
//                ->where('Ration_net', '=', 0)
                ->select(
                    NewCustomerRatio::raw('DATE_FORMAT(DateTime,\'%b %Y\') as date'),
                    'Ration_buy as buy',
                    'Ration_sell as sell',
                    'Ration_pawn as pawn',
                    'Ration_collect as collect'
                )
                ->get();
            return response()->json([
                'error' => 0,
                'data' => $data,
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function customerSegmentation()
    {
        try{
            $data = CustomerSegmentation::query()
//                ->where('Segment', '=', '')
                ->select('Segment', CustomerSegmentation::raw('count(*) as total'))
                ->groupBy('Segment')
                ->get();

            $max = $data->max('total');

            return response()->json([
                'error' => 0,
                'data' => $data,
                'max' => $max
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function monthlyOrderCount()
    {
        try{
            $data = MonthlyTotalNumberOfOrder::query()
//                ->where('net', '=', '0')
                ->select(
                    MonthlyTotalNumberOfOrder::raw('DATE_FORMAT(InvoiceDate,\'%b %Y\') as date'),
                    MonthlyTotalNumberOfOrder::raw('FORMAT(net, 0) as net')
                )
                ->get();

            return response()->json([
                'error' => 0,
                'data' => $data
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function predictingNextPurchaseDayPie()
    {
        try{
            $data = PredictingNextPurchaseDay::query()
//                ->where('CustomerID', '=', '0')
                ->select('Cluster', PredictingNextPurchaseDay::raw('count(*) as total'))
                ->groupBy('Cluster')
                ->get();

            return response()->json([
                'error' => 0,
                'data' => $data
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function predictingNextPurchaseDayStacked()
    {
        try{
            // Query
            $query = PredictingNextPurchaseDay::query()
//                ->where('CustomerID', '=', '0')
                ->select(
                    'Cluster',
                    'Segment_Low-Value as low',
                    'Segment_Mid-Value as mid',
                    'Segment_High-Value as high',
                    PredictingNextPurchaseDay::raw('count(*) as total')
                )
                ->groupBy('Cluster')
                ->groupBy('low')
                ->groupBy('mid')
                ->groupBy('high')
                ->get();

            // Summary
            $data = [];
            $low_count = 0;
            $mid_count = 0;
            $high_count = 0;
            foreach($query as $row) {
                if($row->low != 0) {
                    $low_count += $row->total;
                    $data[$row->Cluster]['low'] = $low_count;
                    $data[$row->Cluster]['name'] = $row->Cluster;
                }
                else if($row->mid != 0) {
                    $mid_count += $row->total;
                    $data[$row->Cluster]['mid'] = $mid_count;
                    $data[$row->Cluster]['name'] = $row->Cluster;
                }
                else if($row->high != 0) {
                    $high_count += $row->total;
                    $data[$row->Cluster]['high'] = $high_count;
                    $data[$row->Cluster]['name'] = $row->Cluster;
                }
            }

            return response()->json([
                'error' => 0,
                'data' => $data,
                'count' => count($data)
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }

    public function predictingNextPurchaseDayList()
    {
        try{
            $data = PredictingNextPurchaseDay::query()
//                ->where('CustomerID', '=', '0')
                ->select(
                    'CustomerID',
                    'Cluster',
                    'Segment_Low-Value as low',
                    'Segment_Mid-Value as mid',
                    'Segment_High-Value as high'
                )
                ->orderBy('Cluster', 'asc')
                ->get();

            return response()->json([
                'error' => 0,
                'data' => $data
            ],'200');

        }catch (\Throwable $e){
            return response()->json([
                'error' => 1,
                'data' => $e,
            ], '200');
        }
    }
}
