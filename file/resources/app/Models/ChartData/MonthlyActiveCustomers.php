<?php

namespace App\Models\ChartData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MonthlyActiveCustomers extends Model
{
    use HasFactory;

    protected $table = '803_MonthlyActiveCustomers';
    protected $fillable = [
        'InvoiceDate',
        'net',
        'pawn',
        'collect',
        'sell',
        'buy',
    ];
}
