<?php

namespace App\Models\ChartData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerLifeTimeValue extends Model
{
    use HasFactory;

    protected $table = '816_CustomerLifeTimeValue';
    protected $fillable = [
        'CustomerID',
        '1y_Revenue',
        'm9_Revenue',
        'm6_Revenue',
        'm3_Revenue',
    ];
}
