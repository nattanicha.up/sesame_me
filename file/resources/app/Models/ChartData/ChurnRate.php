<?php

namespace App\Models\ChartData;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChurnRate extends Model
{
    use HasFactory;

    protected $table = '811_ChurnRate';
    protected $fillable = [
        'ChurnRate_net',
        'ChurnRate_pawn',
        'ChurnRate_collect',
        'ChurnRate_sell',
        'ChurnRate_buy',
        'DateTime',
    ];
}
