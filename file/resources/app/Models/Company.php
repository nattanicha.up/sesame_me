<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $table = '500_COMPANY';
    protected $fillable = [
        'company_name',
        'company_tax',
        'address',
        'province_id',
        'district_id',
        'subdistrict_id',
        'zipcode',
        'telephone',
        'email',
        'website',
        'facebook',
        'line',
        'pic',
        'package_id',
        'status_paid',
        'expired_at',
    ];
}
