@extends('layouts/contentLayoutMasterMain')

@section('title', 'Invoice Edit')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('vendors/css/pickers/flatpickr/flatpickr.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/forms/select/select2.min.css')}}">
@endsection
@section('page-style')
<link rel="stylesheet" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
<link rel="stylesheet" href="{{asset('css/base/pages/app-invoice.css')}}">
@endsection

@section('content')
<section class="invoice-add-wrapper">
  <div class="row invoice-add">
    <!-- Invoice Add Left starts -->
    <div class="col-xl-9 col-md-8 col-12">
      <div class="card invoice-preview-card">
        <!-- Header starts -->
        <div class="card-body invoice-padding pb-0">
          <div class="d-flex justify-content-between flex-md-row flex-column invoice-spacing mt-0">
            <div>
                <div class="invoice-number-date mt-md-0 mt-2">
                    <div class="d-flex align-items-center justify-content-md-end mb-1">
                        <span class="title">ชื่อลูกค้า :</span>
                        <select class="form-control input-group input-group-merge invoice-edit-input-group" id="basicSelect">
                            <option></option>
                        </select>
                    </div>
                    <div class="d-flex align-items-center mb-1">
                        <span class="title">รายละเอียด :</span>
                        <div class="input-group input-group-merge invoice-edit-input-group">
                            <textarea
                                class="form-control"
                                id="exampleFormControlTextarea1"
                                rows="3"
                                placeholder="Textarea"
                            ></textarea>
                        </div>
                    </div>
                    <div class="d-flex align-items-center mb-1">
                        <span class="title">เลขประจำตัวผู้เสียภาษี :</span>
                        <div class="input-group input-group-merge invoice-edit-input-group">
                            <input type="text" class="form-control invoice-edit-input" />
                        </div>
                    </div>
                    <div class="d-flex align-items-center">
                        <span class="title">สำนักงาน/สาขาเลขที่ :</span>
                        <div class="input-group input-group-merge invoice-edit-input-group">
                            <input type="text" class="form-control invoice-edit-input" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="invoice-number-date mt-md-0 mt-2">
              <div class="d-flex align-items-center justify-content-md-end mb-1">
                <span class="invoice-title">วันที่ :</span>
                  <div class="input-group input-group-merge invoice-edit-input-group">
                      <input type="text" class="form-control invoice-edit-input due-date-picker" />
                  </div>
              </div>
              <div class="d-flex align-items-center mb-1">
                <span class="title">เครดิต (วัน) :</span>
                  <div class="input-group input-group-merge invoice-edit-input-group">
                      <input type="text" class="form-control invoice-edit-input" />
                  </div>
              </div>
              <div class="d-flex align-items-center mb-1">
                <span class="title">ครบกำหนด :</span>
                <input type="text" class="form-control invoice-edit-input due-date-picker" />
              </div>
                <div class="d-flex align-items-center">
                    <span class="title">พนักงานขาย :</span>
                    <div class="input-group input-group-merge invoice-edit-input-group">
                        <input type="text" class="form-control invoice-edit-input" />
                    </div>
                </div>
            </div>
          </div>
        </div>
        <!-- Header ends -->

        <hr class="invoice-spacing" />

        <!-- Address and Contact starts -->
        <div class="card-body invoice-padding pt-0">
          <div class="row row-bill-to invoice-spacing">
{{--            <div class="col-xl-4 mb-lg-1 col-bill-to pl-0">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-5">--}}
{{--                        <h6 class="invoice-to-title">ชื่อโปรเจ็ค :</h6>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-7">--}}
{{--                        <div class="invoice-customer">--}}
{{--                            <input type="text" class="form-control invoice-edit-input" />--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-xl-4 pr-0 mt-xl-0 mt-2">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-5">--}}
{{--                        <h6 class="invoice-to-title">เลขที่อ้างอิง :</h6>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-7">--}}
{{--                        <div class="invoice-customer">--}}
{{--                            <input type="text" class="form-control invoice-edit-input" />--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--              <div class="col-xl-4 pr-0 mt-xl-0 mt-2">--}}
{{--                  <div class="row">--}}
{{--                      <div class="col-md-5">--}}
{{--                          <h6 class="invoice-to-title">ราคาสินค้า :</h6>--}}
{{--                      </div>--}}
{{--                      <div class="col-md-7">--}}
{{--                          <div class="invoice-customer">--}}
{{--                              <input type="text" class="form-control invoice-edit-input" />--}}
{{--                          </div>--}}
{{--                      </div>--}}
{{--                  </div>--}}
{{--              </div>--}}
              <div class="col-xl-12 pr-0 mt-xl-0 mt-2">
                  <div class="row">
                      <h6 class="invoice-to-title mr-2">ชื่อโปรเจ็ค :</h6>
                      <div class="invoice-customer">
                          <input type="text" class="form-control invoice-edit-input mr-3" />
                      </div>
                      <h6 class="invoice-to-title mr-2">เลขที่อ้างอิง :</h6>
                      <div class="invoice-customer">
                          <input type="text" class="form-control invoice-edit-input mr-3" />
                      </div>
                      <h6 class="invoice-to-title mr-2">ราคาสินค้า :</h6>
                      <div class="invoice-customer">
                          <input type="text" class="form-control invoice-edit-input mb-0" />
                      </div>
                  </div>
              </div>
          </div>
        </div>
        <!-- Address and Contact ends -->

          <div class="table-responsive mt-0">
              <table class="table">
                  <thead>
                  <tr>
                      <th class="py-1" style="font-size: 16px;">ลำดับ</th>
                      <th class="py-1" style="font-size: 16px;">ชื่อสินค้า/รายละเอียด</th>
                      <th class="py-1" style="font-size: 16px;">จำนวน</th>
                      <th class="py-1" style="font-size: 16px;">หน่วย</th>
                      <th class="py-1" style="font-size: 16px;">ราคาต่อหน่วย</th>
                      <th class="py-1" style="font-size: 16px;">ราคารวม</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr class="border-bottom">
                      <td class="py-1">
                          <p class="card-text font-weight-bold mb-25">1</p>
                      </td>
                      <td class="py-1">
                          <input type="text" class="form-control invoice-edit-input" />
                      </td>
                      <td class="py-1">
                          <input type="text" class="form-control invoice-edit-input" />
                      </td>
                      <td class="py-1">
                          <input type="text" class="form-control invoice-edit-input" />
                      </td>
                      <td class="py-1">
                          <input type="text" class="form-control invoice-edit-input" />
                      </td>
                      <td class="py-1">
                          <span class="font-weight-bold">0.00</span>
                      </td>
                  </tr>
                  <tr class="border-bottom">
                      <td class="py-1">
                          <p class="card-text font-weight-bold mb-25">2</p>
                      </td>
                      <td class="py-1">
                          <input type="text" class="form-control invoice-edit-input" />
                      </td>
                      <td class="py-1">
                          <input type="text" class="form-control invoice-edit-input" />
                      </td>
                      <td class="py-1">
                          <input type="text" class="form-control invoice-edit-input" />
                      </td>
                      <td class="py-1">
                          <input type="text" class="form-control invoice-edit-input" />
                      </td>
                      <td class="py-1">
                          <span class="font-weight-bold">0.00</span>
                      </td>
                  </tr>
                  <tr class="border-bottom">
                      <td class="py-1">
                          <p class="card-text font-weight-bold mb-25">3</p>
                      </td>
                      <td class="py-1">
                          <input type="text" class="form-control invoice-edit-input" />
                      </td>
                      <td class="py-1">
                          <input type="text" class="form-control invoice-edit-input" />
                      </td>
                      <td class="py-1">
                          <input type="text" class="form-control invoice-edit-input" />
                      </td>
                      <td class="py-1">
                          <input type="text" class="form-control invoice-edit-input" />
                      </td>
                      <td class="py-1">
                          <span class="font-weight-bold">0.00</span>
                      </td>
                  </tr>
                  </tbody>
              </table>
          </div>
          <div class="row mt-1 ml-3">
              <div class="col-12 px-0">
                  <button type="button" class="btn btn-primary btn-sm btn-add-new" data-repeater-create>
                      <i data-feather="plus" class="mr-25"></i>
                      <span class="align-middle">เพิ่มรายการ</span>
                  </button>
              </div>
          </div>

        <!-- Invoice Total starts -->
        <div class="card-body invoice-padding">
          <div class="row invoice-sales-total-wrapper">
            <div class="col-md-6 order-md-1 order-2 mt-md-0 mt-3">
              <div class="d-flex align-items-center mb-1">
                  <div class="row">
                      <div class="col-md-4">
                          <label for="salesperson" class="form-label">พนักงานรับออม:</label>
                      </div>
                      <div class="col-md-8">
                          <input type="text" class="form-control ml-50" id="salesperson" />
                      </div>
                  </div>
{{--                <label for="salesperson" class="form-label">พนักงานรับออม:</label>--}}
{{--                <input type="text" class="form-control ml-50" id="salesperson" />--}}
              </div>
            </div>
              <div class="col-md-6 d-flex justify-content-end order-md-2 order-1">
                  <div class="invoice-total-wrapper">
                      <div class="invoice-total-item">
                          <p class="invoice-total-title">ยอดรวม:</p>
                          <p class="invoice-total-amount">10,000</p>
                      </div>
                      <div class="invoice-total-item">
                          <div class="row">
                              <div class="col-md-6">
                                  <p class="invoice-total-title">ส่วนลด:</p>
                              </div>
                              <div class="col-md-4">
                                  <input type="text" class="form-control invoice-edit-input ml-2" />
                              </div>
                              <div class="col-md-2">
                                  <p class="invoice-total-amount">%</p>
                              </div>
                          </div>
                      </div>
                      <div class="invoice-total-item">
                          <p class="invoice-total-title">ภาษี(7%):</p>
                          <p class="invoice-total-amount">9,300</p>
                      </div>
                      <hr class="my-50" />
                      <div class="invoice-total-item">
                          <p class="invoice-total-title">ยอดรวมสุทธิ:</p>
                          <p class="invoice-total-amount">10,000</p>
                      </div>
                  </div>
              </div>
          </div>
        </div>
        <!-- Invoice Total ends -->

        <hr class="invoice-spacing mt-0" />

        <div class="card-body invoice-padding pt-0">
          <!-- Invoice Note starts -->
          <div class="row">
            <div class="col-12">
                <span class="font-weight-bold">Note:</span>
                <span>โปรดรักษาเอกสารฉบับนี้สำหรับเป็นหลักฐานการชำระเงิน</span>
            </div>
          </div>
          <!-- Invoice Note ends -->
        </div>
      </div>
    </div>
    <!-- Invoice Add Left ends -->

    <!-- Invoice Add Right starts -->
    <div class="col-xl-3 col-md-4 col-12">
      <div class="card">
        <div class="card-body">
          <button type="button" class="btn btn-primary btn-block mb-75">พิมพ์</button>
          <a href="{{url('app/invoice/preview')}}" class="btn btn-outline-secondary btn-block mb-75">บันทึก</a>
          <button type="button" class="btn btn-success btn-block">ปิด</button>
        </div>
      </div>
    </div>
    <!-- Invoice Add Right ends -->
  </div>

  <!-- Add New Customer Sidebar -->
  <div class="modal modal-slide-in fade" id="add-new-customer-sidebar" aria-hidden="true">
    <div class="modal-dialog sidebar-lg">
      <div class="modal-content p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
        <div class="modal-header mb-1">
          <h5 class="modal-title">
            <span class="align-middle">Add Customer</span>
          </h5>
        </div>
        <div class="modal-body flex-grow-1">
          <form>
            <div class="form-group">
              <label for="customer-name" class="form-label">Customer Name</label>
              <input type="text" class="form-control" id="customer-name" placeholder="John Doe" />
            </div>
            <div class="form-group">
              <label for="customer-email" class="form-label">Email</label>
              <input type="email" class="form-control" id="customer-email" placeholder="example@domain.com" />
            </div>
            <div class="form-group">
              <label for="customer-address" class="form-label">Customer Address</label>
              <textarea
                class="form-control"
                id="customer-address"
                cols="2"
                rows="2"
                placeholder="1307 Lady Bug Drive New York"
              ></textarea>
            </div>
            <div class="form-group position-relative">
              <label for="customer-country" class="form-label">Country</label>
              <select class="form-control" id="customer-country" name="customer-country">
                <option label="select country"></option>
                <option value="Australia">Australia</option>
                <option value="Canada">Canada</option>
                <option value="Russia">Russia</option>
                <option value="Saudi Arabia">Saudi Arabia</option>
                <option value="Singapore">Singapore</option>
                <option value="Sweden">Sweden</option>
                <option value="Switzerland">Switzerland</option>
                <option value="United Kingdom">United Kingdom</option>
                <option value="United Arab Emirates">United Arab Emirates</option>
                <option value="United States of America">United States of America</option>
              </select>
            </div>
            <div class="form-group">
              <label for="customer-contact" class="form-label">Contact</label>
              <input type="number" class="form-control" id="customer-contact" placeholder="763-242-9206" />
            </div>
            <div class="form-group d-flex flex-wrap mt-2">
              <button type="button" class="btn btn-primary mr-1" data-dismiss="modal">Add</button>
              <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /Add New Customer Sidebar -->
</section>
@endsection

@section('vendor-script')
<script src="{{asset('vendors/js/forms/repeater/jquery.repeater.min.js')}}"></script>
<script src="{{asset('vendors/js/forms/select/select2.full.min.js')}}"></script>
<script src="{{asset('vendors/js/pickers/flatpickr/flatpickr.min.js')}}"></script>
@endsection

@section('page-script')
<script src="{{asset('js/scripts/pages/app-invoice.js')}}"></script>
@endsection
