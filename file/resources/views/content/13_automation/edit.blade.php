@extends('layouts/contentLayoutMasterMain')

@section('title', 'แก้ไขแคมเปญ')

@section('vendor-style')
  <!-- vendor css files -->
  <link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
@endsection
@section('page-style')
  <!-- Page css files -->
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
@endsection

@section('content')
<!-- account setting page -->
<section id="page-account-settings">
  <div class="row">
    <!-- right content section -->
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <div class="tab-content">
            <!-- general tab -->
            <div
              role="tabpanel"
              class="tab-pane active"
              id="account-vertical-general"
              aria-labelledby="account-pill-general"
              aria-expanded="true"
            >
                <!-- header media -->
                <div class="col-12 ml-0 mb-2 pl-0">
                    <h4><i data-feather='edit'></i> <strong>แก้ไขแคมเปญ</strong></h4>
                </div>
                <!--/ header media -->

                  <!-- form -->
                  <form class="validate-form mt-2">
                      <div class="row">
                          <div class="col-12 col-md-5 align-items-center mb-2 mb-md-0">
                              <div class="form-group">
                                  <label for="campaign-no">รหัสแคมเปญ</label>
                                  <input
                                      type="text"
                                      class="form-control"
                                      id="campaign-no"
                                      name="campaign-no"
                                      placeholder=""
                                      value="c2021100613057"
                                      readonly
                                  />
                              </div>
                              <div class="form-group">
                                  <label for="campaign-no">ชื่อแคมเปญ</label>
                                  <input
                                      type="text"
                                      class="form-control"
                                      id="campaign-name"
                                      name="campaign-name"
                                      placeholder=""
                                      value="ทองรูปพรรณลด 10%"
                                  />
                              </div>
                              <div class="form-group">
                                  <label for="campaign-start">วันที่เริ่ม Campaign</label>
                                  <input
                                      type="text"
                                      id="campaign-start"
                                      name="campaign-start"
                                      class="form-control flatpickr-basic"
                                      placeholder="YYYY-MM-DD"
                                      value="2021-11-01"
                                  />
                              </div>
                              <div class="form-group">
                                  <label for="campaign-end">วันที่สิ้นสุด Campaign</label>
                                  <input
                                      type="text"
                                      id="campaign-end"
                                      name="campaign-end"
                                      class="form-control flatpickr-basic"
                                      placeholder="YYYY-MM-DD"
                                      value="2021-11-30"
                                  />
                              </div>
                              <div class="form-group">
                                  <label for="campaign-type">ประเภทแคมเปญ</label>
                                  <select class="form-control" id="campaign-type" name="campaign-type">
                                      <option>ส่วนลด (บาท)</option>
                                      <option selected>ส่วนลด (%)</option>
                                  </select>
                              </div>
                              <div class="form-group">
                                  <label for="campaign-disscount">ส่วนลด</label>
                                  <input
                                      type="text"
                                      class="form-control"
                                      id="campaign-disscount"
                                      name="campaign-disscount"
                                      placeholder=""
                                      value="10"
                                  />
                              </div>
                              <div class="row">
                                  <label class="col-12 custom-control-label mb-1" for="campaign-group">กลุ่มลูกค้า</label>
                                  <div class="col-12 col-sm-6">
                                      <div class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input" id="campaign-group-1" name="campaign-group-1" checked/>
                                          <label class="custom-control-label" for="campaign-group-1">About To Sleep</label>
                                      </div>
                                  </div>
                                  <div class="col-12 col-sm-6">
                                      <div class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input" id="campaign-group-2" name="campaign-group-2"/>
                                          <label class="custom-control-label" for="campaign-group-2">At Risk</label>
                                      </div>
                                  </div>
                                  <div class="col-12 col-sm-6">
                                      <div class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input" id="campaign-group-3" name="campaign-group-3"/>
                                          <label class="custom-control-label" for="campaign-group-3">Cannot Lose Them</label>
                                      </div>
                                  </div>
                                  <div class="col-12 col-sm-6">
                                      <div class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input" id="campaign-group-4" name="campaign-group-4"/>
                                          <label class="custom-control-label" for="campaign-group-4">Champions</label>
                                      </div>
                                  </div>
                                  <div class="col-12 col-sm-6">
                                      <div class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input" id="campaign-group-5" name="campaign-group-5"/>
                                          <label class="custom-control-label" for="campaign-group-5">Hibernating</label>
                                      </div>
                                  </div>
                                  <div class="col-12 col-sm-6">
                                      <div class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input" id="campaign-group-6" name="campaign-group-6"/>
                                          <label class="custom-control-label" for="campaign-group-6">Loyal Customers</label>
                                      </div>
                                  </div>
                                  <div class="col-12 col-sm-6">
                                      <div class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input" id="campaign-group-7" name="campaign-group-7"/>
                                          <label class="custom-control-label" for="campaign-group-7">Need Attention</label>
                                      </div>
                                  </div>
                                  <div class="col-12 col-sm-6">
                                      <div class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input" id="campaign-group-8" name="campaign-group-8"/>
                                          <label class="custom-control-label" for="campaign-group-8">New Customers</label>
                                      </div>
                                  </div>
                                  <div class="col-12 col-sm-6">
                                      <div class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input" id="campaign-group-9" name="campaign-group-9"/>
                                          <label class="custom-control-label" for="campaign-group-9">Potential</label>
                                      </div>
                                  </div>
                                  <div class="col-12 col-sm-6">
                                      <div class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input" id="campaign-group-10" name="campaign-group-10"/>
                                          <label class="custom-control-label" for="campaign-group-10">Promising</label>
                                      </div>
                                  </div>
                                  <div class="col-12 col-sm-6">
                                      <div class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input" id="campaign-group-11" name="campaign-group-11"/>
                                          <label class="custom-control-label" for="campaign-group-11">Unknow</label>
                                      </div>
                                  </div>
                              </div>
                              <div class="row mt-1">
                                  <label class="col-12 custom-control-label mb-1" for="campaign-channel">ช่องทองโฆษณา</label>
                                  <div class="col-12 col-sm-4">
                                      <div class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input" id="campaign-channel-1" name="campaign-channel-1"/>
                                          <label class="custom-control-label" for="campaign-channel-1">Email</label>
                                      </div>
                                  </div>
                                  <div class="col-12 col-sm-4">
                                      <div class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input" id="campaign-channel-2" name="campaign-channel-2" checked/>
                                          <label class="custom-control-label" for="campaign-channel-2">SMS</label>
                                      </div>
                                  </div>
                                  <div class="col-12 col-sm-4">
                                      <div class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input" id="campaign-channel-3" name="campaign-channel-3" checked/>
                                          <label class="custom-control-label" for="campaign-channel-3">LINE</label>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="col-12 col-md-7">
                              <!-- single file upload starts -->
                              <div class="form-group">
                                  <label for="campaign-no">Image Ads</label>
                                  <div class="col-12 mt-1">
                                      <div action="#" class="dropzone dropzone-area" id="dpz-single-file">
                                          <div class="dz-message">Drop files here or click to upload.</div>
                                      </div>
                                  </div>
                              </div>
                              <!-- single file upload ends -->
                              <div class="form-group">
                                  <label for="campaign-no">Campaign URL</label>
                                  <input
                                      type="text"
                                      class="form-control"
                                      id="campaign-url"
                                      name="campaign-url"
                                      placeholder=""
                                      value=""
                                  />
                              </div>
                              <div class="form-group">
                                  <label for="campaign-email-title">Email Title (หัวข้อ)</label>
                                  <input
                                      type="text"
                                      class="form-control"
                                      id="campaign-email-title"
                                      name="campaign-email-title"
                                      placeholder=""
                                      value=""
                                  />
                              </div>
                              <div class="form-group">
                                  <label for="campaign-email-detail">Email Detail (ข้อความ)</label>
                                  <textarea
                                      class="form-control"
                                      id="campaign-email-detail"
                                      name="campaign-email-detail"
                                      rows="3"
                                      placeholder="Textarea"
                                  ></textarea>
                              </div>
                              <div class="form-group">
                                  <label for="campaign-email-sms">SMS Detail (ข้อความ)</label>
                                  <textarea
                                      class="form-control"
                                      id="campaign-email-sms"
                                      name="campaign-email-sms"
                                      rows="3"
                                      placeholder="Textarea"
                                  >หากซื้อทองรูปพรรณ ภายใน 30 พ.ย. 2564 นี้รับส่วนลดทันที 10%</textarea>
                              </div>
                              <div class="form-group">
                                  <label for="campaign-email-line">LINE Detail (ข้อความ)</label>
                                  <textarea
                                      class="form-control"
                                      id="campaign-email-detail-line"
                                      name="campaign-email-detail-line"
                                      rows="3"
                                      placeholder="Textarea"
                                  >หากซื้อทองรูปพรรณ ภายใน 30 พ.ย. 2564 นี้รับส่วนลดทันที 10%</textarea>
                              </div>
                          </div>
                      </div>
                    <div class="row">
                      <div class="col-12 text-right">
                        <button type="submit" class="btn btn-primary mt-2 mr-1">บันทึก</button>
                        <button type="button" class="btn btn-outline-secondary mt-2" onclick="javascript:window.location.href = '{{ url('/automation') }}';">ยกเลิก</button>
                      </div>
                    </div>
                  </form>
                  <!--/ form -->
            </div>
            <!--/ general tab -->
          </div>
        </div>
      </div>
    </div>
    <!--/ right content section -->
  </div>
</section>
<!-- / account setting page -->
@endsection

@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
@endsection
@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/pages/page-account-settings.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
@endsection
