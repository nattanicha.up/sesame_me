@extends('layouts/contentLayoutMasterMain')

@section('title', 'แก้ไขแคมเปญ')

@section('vendor-style')
  <!-- vendor css files -->
  <link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
@endsection
@section('page-style')
  <!-- Page css files -->
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
@endsection

@section('content')
<!-- account setting page -->
<section id="page-account-settings">
  <div class="row">
    <!-- right content section -->
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <div class="tab-content">
            <!-- general tab -->
            <div
              role="tabpanel"
              class="tab-pane active"
              id="account-vertical-general"
              aria-labelledby="account-pill-general"
              aria-expanded="true"
            >
                <!-- header media -->
                <div class="col-12 ml-0 mb-2 pl-0">
                    <h4><i data-feather='edit'></i> <strong>แก้ไขแคมเปญ</strong></h4>
                </div>
                <!--/ header media -->

                  <!-- form -->
                  <form class="validate-form mt-2">
                      <div class="row">
                          <div class="col-12 col-md-5 align-items-center mb-2 mb-md-0">
                              <div class="form-group">
                                  <label for="campaign-no">รหัสแคมเปญ</label>
                                  <div id="campaign-no">c2021100613057</div>
                              </div>
                              <div class="form-group">
                                  <label for="campaign-no">ชื่อแคมเปญ</label>
                                  <div id="campaign-name">ทองรูปพรรณลด 10%</div>
                              </div>
                              <div class="form-group">
                                  <label for="campaign-start">วันที่เริ่ม Campaign</label>
                                  <div id="campaign-start">2021-11-01</div>
                              </div>
                              <div class="form-group">
                                  <label for="campaign-end">วันที่สิ้นสุด Campaign</label>
                                  <div id="campaign-no">2021-11-30</div>
                              </div>
                              <div class="form-group">
                                  <label for="campaign-type">ประเภทแคมเปญ</label>
                                  <div id="campaign-no">ส่วนลด (%)</div>
                              </div>
                              <div class="form-group">
                                  <label for="campaign-type">ส่วนลด</label>
                                  <div id="campaign-disscount">10</div>
                              </div>
                              <div class="row">
                                  <label class="col-12" for="campaign-group">กลุ่มลูกค้า</label>
                                  <div id="campaign-group" class="ml-1">About To Sleep</div>
                              </div>
                              <div class="row mt-1">
                                  <label class="col-12" for="campaign-channel">ช่องทองโฆษณา</label>
                                  <div id="campaign-group" class="ml-1">SMS, LINE</div>
                              </div>
                          </div>
                          <div class="col-12 col-md-7">
                              <!-- single file upload starts -->
                              <div class="form-group">
                                  <label for="campaign-no">Image Ads</label>
                                  <div id="campaign-image" class="text-center mt-3 mb-3"><img src="{{ asset('images/goldbar.jpg') }}"/></div>
                              </div>
                              <!-- single file upload ends -->
                              <div class="form-group">
                                  <label for="campaign-no">Campaign URL</label>
                                  <div id="campaign-url">-</div>
                              </div>
                              <div class="form-group">
                                  <label for="campaign-email-title">Email Title (หัวข้อ)</label>
                                  <div id="campaign-email-title">-</div>
                              </div>
                              <div class="form-group">
                                  <label for="campaign-email-detail">Email Detail (ข้อความ)</label>
                                  <div id="campaign-email-detail">-</div>
                              </div>
                              <div class="form-group">
                                  <label for="campaign-sms-detail">SMS Detail (ข้อความ)</label>
                                  <div id="campaign-sms-detail">หากซื้อทองรูปพรรณ ภายใน 30 พ.ย. 2564 นี้รับส่วนลดทันที 10%</div>
                              </div>
                              <div class="form-group">
                                  <label for="campaign-email-line">LINE Detail (ข้อความ)</label>
                                  <div id="campaign-line-detail">หากซื้อทองรูปพรรณ ภายใน 30 พ.ย. 2564 นี้รับส่วนลดทันที 10%</div>
                              </div>
                          </div>
                      </div>
                    <div class="row">
                      <div class="col-12 text-right">
                        {{--<button type="submit" class="btn btn-primary mt-2 mr-1">บันทึก</button>
                        <button type="button" class="btn btn-outline-secondary mt-2" onclick="javascript:window.location.href = '{{ url('/automation') }}';">ยกเลิก</button>--}}
                        <button type="button" class="btn btn-primary mt-2" onclick="javascript:window.location.href = '{{ url('/automation') }}';">ยกเลิก</button>
                      </div>
                    </div>
                  </form>
                  <!--/ form -->
            </div>
            <!--/ general tab -->
          </div>
        </div>
      </div>
    </div>
    <!--/ right content section -->
  </div>
</section>
<!-- / account setting page -->
@endsection

@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
@endsection
@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/pages/page-account-settings.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
@endsection
