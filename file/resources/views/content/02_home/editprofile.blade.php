@extends('layouts/contentLayoutMasterMain')

{{--@section('title', 'Statistics Cards')--}}


@section('vendor-style')

    <!-- vendor css files -->
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection
@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')
    <section id="page-account-settings">
        <div class="row">
            <!-- right content section -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <!-- header media -->
                        <div class="media">
                            <a href="javascript:void(0);" class="mr-25">
                                <img
                                    src="{{asset('images/portrait/small/avatar-s-11.jpg')}}"
                                    id="account-upload-img"
                                    class="rounded mr-50"
                                    alt="profile image"
                                    height="80"
                                    width="80"
                                />
                            </a>
                            <!-- upload and reset button -->
                            <div class="media-body mt-75 ml-1">
                                <label for="account-upload" class="btn btn-sm btn-primary mb-75 mr-75">Upload</label>
                                <input type="file" id="account-upload" hidden accept="image/*" />
                                <button class="btn btn-sm btn-outline-secondary mb-75">Reset</button>
                                <p>Allowed JPG, GIF or PNG. Max size of 800kB</p>
                            </div>
                        </div>

                        <form class="validate-form mt-2">
                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="tokenId">Token Id</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="tokenId"
                                            name="tokenId"
                                            placeholder="Token Id"
                                            value="U65d9cc8e0af58ab5d9cdd92514d7e099"
                                        />
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="account-name">Name</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="account-name"
                                            name="name"
                                            placeholder="Name"
                                            value="John Doe"
                                        />
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="account-e-mail">E-mail</label>
                                        <input
                                            type="email"
                                            class="form-control"
                                            id="account-e-mail"
                                            name="email"
                                            placeholder="Email"
                                            value="granger007@hogward.com"
                                        />
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="account-tel">Tel</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="account-tel"
                                            name="company"
                                            placeholder="Telephone"
                                            value="095-8526321"
                                        />
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary mt-2 mr-1">บันทึก</button>
{{--                                    <button type="reset" class="btn btn-outline-secondary mt-2">Cancel</button>--}}
                                </div>
                            </div>
                        </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('vendor-script')
    {{-- Vendor js files --}}
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/pages/page-account-settings.js')) }}"></script>
@endsection

@section('vendor-script')


@endsection
