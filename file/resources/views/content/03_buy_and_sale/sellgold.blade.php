@extends('layouts/contentLayoutSellGold')

@section('title', 'ร้านขายออก')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset('css/base/pages/app-ecommerce-sellgold.css') }}">
@endsection

@section('content')
    <section id="input-group-basic">
        <div class="row">
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h2 class="font-weight-bolder mb-0">27,800</h2>
                            <p class="card-text">ซื้อทองคำแท่ง</p>
                        </div>
                        <div class="avatar bg-light-danger p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather='shopping-bag' class="font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h2 class="font-weight-bolder mb-0">27,900</h2>
                            <p class="card-text">ขายทองคำแท่ง</p>
                        </div>
                        <div class="avatar bg-light-success p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather="shopping-cart" class="font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h2 class="font-weight-bolder mb-0">27,300</h2>
                            <p class="card-text">ซื้อทองรูปพรรณ</p>
                        </div>
                        <div class="avatar bg-light-danger p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather="shopping-bag" class="font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h2 class="font-weight-bolder mb-0">28,400</h2>
                            <p class="card-text">ขายทองรูปพรรณ</p>
                        </div>
                        <div class="avatar bg-light-success p-50 m-0">
                            <div class="avatar-content">
                                <i data-feather="shopping-cart" class="font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Basic -->
            <div class="col-md-8">
                <div class="input-group mb-2">
                    <input
                        type="text"
                        class="form-control"
                        placeholder="ค้นหาทอง"
                        aria-label="Search..."
                        aria-describedby="basic-addon-search1"
                    />
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon-search1"><i data-feather="search"></i></span>
                    </div>
                </div>
                <div class="list-view product-checkout">
                    <div class="checkout-items">
                        <div class="text-center mb-2">
{{--                            <button type="button" class="btn btn-success mt-1 remove-wishlist">--}}
{{--                                <i data-feather="plus" class="align-middle mr-25"></i>--}}
{{--                                <span>เพิ่ม</span>--}}
{{--                            </button>--}}
                        </div>
                        <div class="card ecommerce-card">
                            <div class="item-img ml-2 mr-2">
                                <img src="{{asset('images/gold/gold1.jpg')}}" alt="img-placeholder" />
                            </div>
                            <div class="card-body">
                                <div class="item-name">
                                    <h4 class="mb-0 text-body bold">ออสสิริส</h4>
                                    <span class="text-primary">ทองคำแท่ง</span>
                                    <div class="badge badge-pill badge-warning">96.5 %</div>
                                </div>
                                <span class="item-company mt-1">หนัก: 1 บาท</span>
                                <span class="item-company">ขนาด/ความยาว: 1</span>
                                <div class="form-group row mb-0 mt-1">
                                    <label for="colFormLabelLg" class="col-sm-4 col-form-label"><strong>ค่ากำเหน็ด:</strong></label>
                                    <div class="col-sm-5">
                                        <input
                                            type="number"
                                            class="form-control item-company"
                                            id="colFormLabelLg"
                                            placeholder=""
                                            value=""
                                        />
                                    </div>
                                </div>
                            </div>
                            <div class="item-options text-center">
                                <div class="item-wrapper">
                                    <div class="item-cost">
                                        <h4 class="item-price">฿ 25,000</h4>
                                        <span class="item-company">(฿ 5,000)</span>
                                        <p class="card-text shipping mt-1">
                                            <span class="badge badge-pill badge-light-success">คงเหลือ 10 ชิ้น</span>
                                        </p>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-dark mt-1 remove-wishlist mt-3">
                                    <i data-feather="x" class="align-middle mr-25"></i>
                                    <span>ยกเลิก</span>
                                </button>
                            </div>
                        </div>
                        <div class="card ecommerce-card">
                            <div class="item-img ml-2 mr-2">
                                <img src="{{asset('images/gold/gold2.jpg')}}" alt="img-placeholder" />
                            </div>
                            <div class="card-body">
                                <div class="item-name">
                                    <h4 class="mb-0 text-body bold">สปริงหลุยส์</h4>
                                    <span class="text-primary">สร้อยคอ</span>
                                    <div class="badge badge-pill badge-warning">96.5 %</div>
                                </div>
                                <span class="item-company mt-1">หนัก: 2 บาท</span>
                                <span class="item-company">ขนาด/ความยาว: 8</span>
                                <div class="form-group row mb-0 mt-1">
                                    <label for="colFormLabelLg" class="col-sm-4 col-form-label"><strong>ค่ากำเหน็ด:</strong></label>
                                    <div class="col-sm-5">
                                        <input
                                            type="number"
                                            class="form-control item-company"
                                            id="colFormLabelLg"
                                            placeholder=""
                                            value=""
                                        />
                                    </div>
                                </div>
                            </div>
                            <div class="item-options text-center">
                                <div class="item-wrapper">
                                    <div class="item-cost">
                                        <h4 class="item-price">฿ 58,000</h4>
                                        <span class="item-company">(฿ 2,000)</span>
                                        <p class="card-text shipping mt-1">
                                            <span class="badge badge-pill badge-light-success">คงเหลือ 5 ชิ้น</span>
                                        </p>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-dark mt-1 remove-wishlist mt-3">
                                    <i data-feather="x" class="align-middle mr-25"></i>
                                    <span>ยกเลิก</span>
                                </button>
                            </div>
                        </div>
                        <div class="card ecommerce-card">
                            <div class="item-img ml-2 mr-2">
                                <img src="{{asset('images/gold/gold3.jpg')}}" alt="img-placeholder" />
                            </div>
                            <div class="card-body">
                                <div class="item-name">
                                    <h4 class="mb-0 text-body bold">ดอกไม้ฝังพลอย</h4>
                                    <span class="text-primary">แหวน</span>
                                    <div class="badge badge-pill badge-warning">96.5 %</div>
                                </div>
                                <span class="item-company mt-1">หนัก: 1 บาท</span>
                                <span class="item-company">ขนาด/ความยาว: 1</span>
                                <div class="form-group row mb-0 mt-1">
                                    <label for="colFormLabelLg" class="col-sm-4 col-form-label"><strong>ค่ากำเหน็ด:</strong></label>
                                    <div class="col-sm-5">
                                        <input
                                            type="number"
                                            class="form-control item-company"
                                            id="colFormLabelLg"
                                            placeholder=""
                                            value=""
                                        />
                                    </div>
                                </div>
                            </div>
                            <div class="item-options text-center">
                                <div class="item-wrapper">
                                    <div class="item-cost">
                                        <h4 class="item-price">฿ 25,000</h4>
                                        <span class="item-company">(฿ 5,000)</span>
                                        <p class="card-text shipping mt-1">
                                            <span class="badge badge-pill badge-light-success">คงเหลือ 1 ชิ้น</span>
                                        </p>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-dark mt-1 remove-wishlist mt-3">
                                    <i data-feather="x" class="align-middle mr-25"></i>
                                    <span>ยกเลิก</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Merged -->
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <label class="section-label" style="font-size: 16px;">ผู้ขาย</label>
                        <hr />
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                <div class="user-avatar-section">
                                    <div class="d-flex justify-content-start">
                                        <img
                                            class="avatar"
                                            src="{{asset('images/avatars/7.png')}}"
                                            height="104"
                                            width="104"
                                            alt="User avatar"
                                        />
                                        <div class="ml-1">
                                            <div class="user-info mb-1">
                                                <h4 class="mb-0">Eleanor Aguilar</h4>
                                                <span class="card-text" style="font-size: 10px;"><i data-feather='map-pin'></i> 112 หมู่ 3 ต.บ้านปง อ.หางดง จ.เชียงใหม่ 50230</span>
                                                <div class="d-flex flex-wrap mt-0">
                                                    <span class="card-text" style="font-size: 10px;"><i data-feather='phone'></i> 095-451-3731</span>
                                                </div>
                                            </div>

                                            {{--<div class="d-flex flex-wrap">
                                                <a href="{{url('app/user/edit')}}" class="btn btn-primary">แก้ไข</a>
                                                <button class="btn btn-outline-danger ml-1">ลบ</button>
                                            </div>--}}
                                            <button type="reset" class="btn btn-primary mr-1">แก้ไข</button>
                                            <button type="reset" class="btn btn-outline-danger">ลบ</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <label class="section-label" style="font-size: 16px;">ชำระเงิน</label>
                        <hr />
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">ยอดรวม</label>
                                    <div class="col-sm-7 col-form-label col-form-label-lg"><strong>120,000 บาท</strong></div>
                                    <div class="col-sm-2 col-form-label col-form-label-lg"></div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">โค๊ดส่วนลด</label>
                                    <div class="col-sm-7">
                                        <input
                                            type="text"
                                            class="form-control form-control-lg"
                                            id="colFormLabelLg"
                                            placeholder=""
                                            value="ABC0001"
                                        />
                                    </div>
                                    <div class="col-sm-2 col-form-label col-form-label-lg"></div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">ส่วนลด</label>
                                    <div class="col-sm-7">
                                        <input
                                            type="number"
                                            class="form-control form-control-lg"
                                            id="colFormLabelLg"
                                            placeholder=""
                                            value="500"
                                        />
                                    </div>
                                    <div class="col-sm-2 col-form-label col-form-label-lg">บาท</div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">ยอดสุทธิ</label>
                                    <div class="col-sm-7 col-form-label col-form-label-lg"><strong>118,500 บาท</strong></div>
                                    <div class="col-sm-2 col-form-label col-form-label-lg"></div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">รับเงิน</label>
                                    <div class="col-sm-7">
                                        <input
                                            type="number"
                                            class="form-control form-control-lg"
                                            id="colFormLabelLg"
                                            placeholder=""
                                            value="119000"
                                        />
                                    </div>
                                    <div class="col-sm-2 col-form-label col-form-label-lg">บาท</div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">เงินทอน</label>
                                    <div class="col-sm-7 col-form-label col-form-label-lg"><strong>500 บาท</strong></div>
                                    <div class="col-sm-2 col-form-label col-form-label-lg"></div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">ช่องทางการขาย</label>
                                    <div class="col-sm-9 col-form-label col-form-label-lg">
                                        <div class="demo-inline-spacing">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" checked />
                                                <label class="custom-control-label" for="customRadio1">หน้าร้าน</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input" />
                                                <label class="custom-control-label" for="customRadio2">ออนไลน์</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input" />
                                                <label class="custom-control-label" for="customRadio3">ตัวแทน</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-4 col-form-label col-form-label-lg">ช่องทางการชำระเงิน</label>
                                    <div class="col-sm-8 col-form-label col-form-label-lg">
                                        <div class="demo-inline-spacing">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1" checked />
                                                <label class="custom-control-label" for="customCheck1">เงินสด</label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck2" checked />
                                                <label class="custom-control-label" for="customCheck2">เครดิต</label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck3" checked />
                                                <label class="custom-control-label" for="customCheck3">โอนเงิน</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">เงินสด</label>
                                    <div class="col-sm-7">
                                        <input
                                            type="number"
                                            class="form-control form-control-lg"
                                            id="colFormLabelLg"
                                            placeholder=""
                                            value="30000"
                                        />
                                    </div>
                                    <div class="col-sm-2 col-form-label col-form-label-lg">บาท</div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">เครดิต</label>
                                    <div class="col-sm-7">
                                        <input
                                            type="number"
                                            class="form-control form-control-lg"
                                            id="colFormLabelLg"
                                            placeholder=""
                                            value="2000"
                                        />
                                    </div>
                                    <div class="col-sm-2 col-form-label col-form-label-lg">บาท</div>
                                </div>
                                <div class="form-group row">
                                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">โอนเงิน</label>
                                    <div class="col-sm-7">
                                        <input
                                            type="number"
                                            class="form-control form-control-lg"
                                            id="colFormLabelLg"
                                            placeholder=""
                                            value="500"
                                        />
                                    </div>
                                    <div class="col-sm-2 col-form-label col-form-label-lg">บาท</div>
                                </div>
                                <hr />
                                <button type="button" class="btn btn-primary btn-block btn-next place-order" id="confirmPayment">ยืนยันการชำระเงิน</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/form-number-input.js')) }}"></script>

    <script>
        $(document).ready(function() {
            $("#confirmPayment").click(function() {
                window.location.href = "{{ url('/sell-invoice') }}";
            });
        });
    </script>
@endsection
