<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        @font-face {
            font-family: 'tahomabd';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('/fonts/tahomabd.ttf') }}") format('truetype');
        }
        @page { margin: 0px; }
        body {
            font-family: "tahomabd" !important;
            font-size: 6.5pt;
            padding: 0px;
            margin: 0px;
        }
        td{ padding: 5px 0 5px 0; line-height: 5pt}
        .font-thai{
            font-family: "tahomabd" !important;
        }
    </style>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title}}</title>
</head>
<body style="padding-bottom: 0">
    <div class="header" style="text-align: center;  padding-bottom: 5pt;">
        <span style="font-size: 8pt;">{{ Auth::user()->company->company_name }}</span><br>
        <span>{{ Auth::user()->company->address }} {{ Auth::user()->company->subdistrict->title_th }} {{ Auth::user()->company->district->title_th }} {{ Auth::user()->company->province->title_th }} {{ Auth::user()->company->subdistrict->districtCode->zipcode }}</span><br>
        <span>โทร. {{ Auth::user()->company->telephone }}</span><br>
        <span>TAX#{{ Auth::user()->company->company_tax }}</span>
        <p>รับซื้อทอง {{$transectiontrade->transection_id}}</p>
        <p>(วันที่ {{ $transectionDate }})</p>
    </div>
    <div style="border-bottom: 1px solid #444">
        <span class="mb-3 font-thai">รายการสินค้า</span>
    </div>
    <table width="100%" style="margin-bottom: 20px">
        <tr>
            <td>รายการ</td>
            <td style="text-align: right">ราคา</td>
        </tr>
        @foreach($transectiontrade->goldbuy as $buy)
            <tr>
                <td>{{ $buy->title }}</td>
                <td style="text-align: right">{{ number_format($buy->price, 2)  }}</td>
            </tr>
        @endforeach
    </table>
    <div style="border-bottom: 1px solid #444">
        <span class="mb-3 font-thai">ยอดเงิน</span>
    </div>
    <table width="100%" style="margin-bottom: 20px">
    <tr>
        <td>ยอดรวม</td>
        <td style="text-align: right">{{number_format($transectiontrade->total, 2)}}</td>
    </tr>
    <tr>
        <td>ยอดจ่ายเงิน</td>
        <td style="text-align: right">{{number_format($transectiontrade->total, 2)}}</td>
    </tr>
    </table>
    <div style="border-bottom: 1px solid #444">
        <span class="mb-3 font-thai" style="font-size: 8pt">ข้อมูลผู้ขาย</span>
    </div>
    <table width="100%">
        <tr>
            <td>ชื่อ-นามสกุล</td>
            <td style="text-align: right">{{$transectiontrade->customer->name}} {{$transectiontrade->Customer->surname}}</td>
        </tr>
        <tr>
            <td>เบอร์โทรศัพท์</td>
            <td style="text-align: right">{{$transectiontrade->customer->phone}}</td>
        </tr>
        <tr>
            <td>อีเมล</td>
            <td style="text-align: right">{{$transectiontrade->customer->email}}</td>
        </tr>
    </table>
    <div style="border-bottom: 1px solid #444">
        <span class="mb-3 font-thai" style="font-size: 8pt">พนักงานผู้รับซื้อ</span>
    </div>
    <table width="100%" style="margin-bottom: 0">
        <tr>
            <td>ชื่อ-นามสกุล</td>
            <td style="text-align: right">{{$transectiontrade->userAdd->name}} {{$transectiontrade->userAdd->surname}}</td>
        </tr>
    </table>

    <p>&nbsp;</p>
    <div style="width: 100%; border-bottom: #0b0b0b dashed 1px;"></div>

    <div class="header" style="text-align: center;  padding-bottom: 5pt;">
        <span style="font-size: 8pt;">{{ Auth::user()->company->company_name }}</span><br>
        <span>{{ Auth::user()->company->address }} {{ Auth::user()->company->subdistrict->title_th }} {{ Auth::user()->company->district->title_th }} {{ Auth::user()->company->province->title_th }} {{ Auth::user()->company->subdistrict->districtCode->zipcode }}</span><br />
        <span>โทร. {{ Auth::user()->company->telephone }}</span><br>
        <span>TAX#{{ Auth::user()->company->company_tax }}</span>
        <p>รับซื้อทอง {{$transectiontrade->transection_id}}</p>
        <p>(วันที่ {{ $transectionDate }})</p>
    </div>
    <div style="border-bottom: 1px solid #444">
        <span class="mb-3 font-thai">รายการสินค้า</span>
    </div>
    <table width="100%" style="margin-bottom: 20px">
        <tr>
            <td>รายการ</td>
            <td style="text-align: right">ราคา</td>
        </tr>
        @foreach($transectiontrade->goldbuy as $buy)
            <tr>
                <td>{{ $buy->title }}</td>
                <td style="text-align: right">{{ number_format($buy->price, 2)  }}</td>
            </tr>
        @endforeach
    </table>
    <div style="border-bottom: 1px solid #444">
        <span class="mb-3 font-thai">ยอดเงิน</span>
    </div>
    <table width="100%" style="margin-bottom: 20px">
        <tr>
            <td>ยอดรวม</td>
            <td style="text-align: right">{{number_format($transectiontrade->total, 2)}}</td>
        </tr>
        <tr>
            <td>ยอดจ่ายเงิน</td>
            <td style="text-align: right">{{number_format($transectiontrade->total, 2)}}</td>
        </tr>
    </table>
    <div style="border-bottom: 1px solid #444">
        <span class="mb-3 font-thai" style="font-size: 8pt">ข้อมูลผู้ขาย</span>
    </div>
    <table width="100%">
        <tr>
            <td>ชื่อ-นามสกุล</td>
            <td style="text-align: right">{{$transectiontrade->Customer->name}} {{$transectiontrade->Customer->surname}}</td>
        </tr>
        <tr>
            <td>เบอร์โทรศัพท์</td>
            <td style="text-align: right">{{$transectiontrade->Customer->phone}}</td>
        </tr>
        <tr>
            <td>อีเมล</td>
            <td style="text-align: right">{{$transectiontrade->Customer->email}}</td>
        </tr>
    </table>
    <div style="border-bottom: 1px solid #444">
        <span class="mb-3 font-thai" style="font-size: 8pt">พนักงานผู้รับซื้อ</span>
    </div>
    <table width="100%" style="margin-bottom: 0">
        <tr>
            <td>ชื่อ-นามสกุล</td>
            <td style="text-align: right">{{$transectiontrade->userAdd->name}} {{$transectiontrade->userAdd->surname}}</td>
        </tr>
    </table>
</body>
</html>
